import { Component } from '@angular/core';

interface Producto {
  presupuesto: string;
  unidad: string;
  descripcion: string;
  cantidad: number;
  valorUnitario: number;
  valorTotal?: number;
  fechaAdquisicion?: string;
  proveedor?: string;
}


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  productos: Producto[] = JSON.parse(localStorage.getItem('productos') || '[]');
  
  filtroProducto: string = '';
filtroUnidad: string = '';

nuevoProducto() {
  const presupuesto = prompt('Ingrese el monto asignado:');
  const unidad = prompt('Ingrese el departamento responsable:');
  const descripcion = prompt('Ingrese la descripción del producto:');
  const cantidadString = prompt('Ingrese la cantidad del producto:');
  const valorUnitarioString = prompt('Ingrese el valor unitario del producto:');
  
  // Verificar si el valor devuelto por prompt es null
  const cantidad = parseInt(cantidadString ?? '0');
  const valorUnitario = parseFloat(valorUnitarioString ?? '0.0');
  
  const fechaAdquisicion = prompt('Ingrese la fecha de adquisición del producto:');
  const proveedor = prompt('Ingrese el proveedor del producto:');

  if (presupuesto && unidad && descripcion && !isNaN(cantidad) && !isNaN(valorUnitario) && fechaAdquisicion && proveedor) {
    const producto: Producto = { presupuesto, unidad, descripcion, cantidad, valorUnitario, fechaAdquisicion, proveedor };
    this.productos.push(producto);
    localStorage.setItem('productos', JSON.stringify(this.productos));

    // Recargar la página después de guardar el producto
    location.reload();
  } else {
    alert('Ingrese datos válidos en todos los campos.');
  }
}


  editarProducto(producto: Producto) {
    const nuevaDescripcion = prompt('Ingrese la nueva descripción del producto:', producto.descripcion) || '';
    const nuevaCantidad = parseInt(prompt('Ingrese la nueva cantidad del producto:', producto.cantidad.toString()) || '0');
    const nuevoValorUnitario = parseFloat(prompt('Ingrese el nuevo valor unitario del producto:', producto.valorUnitario.toString()) || '0');

    if (nuevaDescripcion && !isNaN(nuevaCantidad) && !isNaN(nuevoValorUnitario)) {
      producto.descripcion = nuevaDescripcion;
      producto.cantidad = nuevaCantidad;
      producto.valorUnitario = nuevoValorUnitario;
      localStorage.setItem('productos', JSON.stringify(this.productos));
    } else {
      alert('Ingrese datos válidos.');
    }
  }

  eliminarProducto(producto: Producto) {
    const confirmacion = confirm('¿Está seguro de eliminar este producto?');
    if (confirmacion) {
      this.productos = this.productos.filter(p => p !== producto);
      localStorage.setItem('productos', JSON.stringify(this.productos));
    }
  }
}
