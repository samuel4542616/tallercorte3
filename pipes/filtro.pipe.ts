import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {
  transform(items: any[], filtroProducto: string, filtroUnidad: string): any[] {
    if (!items) {
      return [];
    }
    
    const filtroProductoLower = filtroProducto ? filtroProducto.toLowerCase() : null;
    const filtroUnidadLower = filtroUnidad ? filtroUnidad.toLowerCase() : null;

    return items.filter(item => {
      const productoMatch = filtroProductoLower ? item.descripcion.toLowerCase().includes(filtroProductoLower) : true;
      const unidadMatch = filtroUnidadLower ? item.unidad.toLowerCase().includes(filtroUnidadLower) : true;
      return productoMatch && unidadMatch;
    });
  }
}
